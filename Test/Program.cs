﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            int input;
            while (true)
            {
                Console.WriteLine("1: Output Test.");
                Console.WriteLine("2: Input Test.");
                Console.WriteLine("0: Quit.");
                Console.WriteLine("Input > ");
                try
                {
                    input = Convert.ToInt32(Console.ReadLine());
                    if (0 <= input || input <= 2)
                    {
                        break;
                    }
                }
                catch
                {
                }
                Console.WriteLine("invalid Input.");
            }

            switch (input)
            {
                case 0:
                    break;

                case 1:
                    OutputTest();
                    break;

                case 2:
                    InputTest();
                    break;
            }
        }

        static void OutputTest()
        {
            var count = MidiSharp.Output.MidiOutputPort.GetPortCount();
            Console.WriteLine("All MIDI Out Port:");
            for (var i = 0; i < count; i++)
            {
                Console.WriteLine("  Port No.{0} : " + MidiSharp.Output.MidiOutputPort.GetPortInfomation(i).ProductName, i);
            }
            int number;
            while (true)
            {
                try
                {
                    Console.Write("Port Number (0～{0}) > ", count - 1);
                    number = Convert.ToInt32(Console.ReadLine());
                    if (0 <= number && number < count)
                    {
                        break;
                    }
                }
                catch
                {
                }
                Console.WriteLine("Invalid Number.");
            }
            using (var h = new MidiSharp.Output.MidiOutputPort(number))
            {
                h.SendMessage(new byte[3] { 0x90, 0x40, 0x40 });
                System.Threading.Thread.Sleep(1000);
                h.SendMessage(new byte[] { 0xF0, 0x7E, 0x7F, 0x09, 0x01, 0xF7 });
                h.SendMessage(new byte[3] { 0x90, 0x40, 0x40 });
                System.Threading.Thread.Sleep(1000);
            }

            Console.WriteLine("Push Enter to Exit.");
            Console.ReadLine();
        }

        static void InputTest()
        {
            var count = MidiSharp.Input.MidiInputPort.GetPortCount();
            if (count > 0)
            {
                Console.WriteLine("All MIDI In Port:");
                for (var i = 0; i < count; i++)
                {
                    Console.WriteLine("  Port No.{0} : " + MidiSharp.Input.MidiInputPort.GetPortInfomation(i).ProductName, i);
                }
                int number;
                while (true)
                {
                    try
                    {
                        Console.Write("Port Number (0～{0}) > ", count - 1);
                        number = Convert.ToInt32(Console.ReadLine());
                        if (0 <= number && number < count)
                        {
                            break;
                        }
                    }
                    catch
                    {
                    }
                    Console.WriteLine("Invalid Number.");
                }
                var h = new MidiSharp.Input.MidiInputPort(number);
                h.MessageReceived += (sender, e) =>
                {
                    foreach (var d in e.Data)
                    {
                        Console.Write("{0:d3}(0x{0:x2}), ", d);
                    }
                    Console.WriteLine(e.TimeStamp);
                };

                h.StartReceiving();
                Console.WriteLine();
                Console.WriteLine("Open Port {0} : {1}.", number, MidiSharp.Input.MidiInputPort.GetPortInfomation(number).ProductName);
                Console.WriteLine("Push Enter to Exit.");
                Console.WriteLine();

                Console.ReadLine();

                h.Dispose();
            }
            else
            {
                Console.WriteLine("MIDI In port is not Found.");
                Console.WriteLine("Push Enter to Exit.");
                Console.ReadLine();
            }
        }
    }
}
