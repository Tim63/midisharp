﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using MidiSharp.Message;
using MidiSharp.Win32;

namespace MidiSharp.Input
{
    public class MidiInputPort : IDisposable
    {
        #region Fields

        IntPtr handle = IntPtr.Zero;

        MidiInProc midiInProc;

        #endregion

        #region Properties

        public MidiInputCaps Capabilities { get { return midiInputCaps; } }
        MidiInputCaps midiInputCaps;

        public string ProductName { get { return midiInputCaps.ProductName; } }

        public bool IsOpen { get { return isOpen; } }
        bool isOpen = false;

        public bool IsReceiving { get { return isReceiving; } }
        bool isReceiving = false;

        public bool IsDisposed { get { return isDisposed; } }
        bool isDisposed = false;

        #endregion

        #region Constructor

        public MidiInputPort(int portNumber)
        {
            midiInputCaps = GetPortInfomation(portNumber);
            midiInProc = new MidiInProc(MidiInProc);

            Open(portNumber);
        }

        private void Open(int portNumber)
        {
            if (!IsOpen)
            {
                CheckMultimediaResult(midiInOpen(out handle, portNumber, midiInProc, 0, MidiCallbackType.Function));
                isOpen = true;
            }
        }

        ~MidiInputPort()
        {
            Dispose(false);
        }

        #endregion

        #region IDisposable

        public void Close()
        {
            StopReceiving();

            if (IsOpen)
            {
                CheckMultimediaResult(midiInClose(handle));
                isOpen = false;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!IsDisposed)
            {
                Close();

                isDisposed = true;
            }
        }

        #endregion

        #region Receive Message

        public void StartReceiving()
        {
            if (!IsReceiving)
            {
                CheckMultimediaResult(midiInStart(handle));
                isReceiving = true;
            }
        }

        public void StopReceiving()
        {
            if (IsReceiving)
            {
                CheckMultimediaResult(midiInStop(handle));
                CheckMultimediaResult(midiInReset(handle));
                isReceiving = false;
            }
        }

        public event EventHandler<MessageReceivedEventArgs> MessageReceived;

        private void OnMessageReceived(MessageReceivedEventArgs e)
        {
            if (MessageReceived != null)
            {
                MessageReceived(this, e);
            }
        }

        private void MidiInProc(IntPtr midiInHandle, int message, int instance, int parameter1, int parameter2)
        {
            switch (message)
            {
                case MultimediaMessage.MidiInputMessage.Data:
                    byte statusByte = (byte)(parameter1 & 0x000000ff);
                    byte dataByte1 = (byte)((parameter1 & 0x0000ff00) >> 8);
                    byte dataByte2 = (byte)((parameter1 & 0x00ff0000) >> 16);
                    OnMessageReceived(new MessageReceivedEventArgs(new[] { statusByte, dataByte1, dataByte2 }, parameter2));
                    break;

                default:
                    break;
            }
        }

        #endregion

        #region Static Methods

        public static int GetPortCount()
        {
            return midiInGetNumDevs();
        }

        public static MidiInputCaps GetPortInfomation(int portNumber)
        {
            var caps = new MidiInputCaps();
            CheckMultimediaResult(midiInGetDevCaps(portNumber, ref caps, Marshal.SizeOf(typeof(MidiInputCaps))));
            return caps;
        }

        private static void CheckMultimediaResult(int result)
        {
            if (result != MultimediaResult.SystemError.NoError)
            {
                var stringBuilder = new StringBuilder(Constants.MaxErrorStringLenght);
                if (midiInGetErrorText(result, stringBuilder, stringBuilder.Capacity) != MultimediaResult.SystemError.NoError)
                {
                    throw new MultimediaException(string.Format("Failed to get error text. Code : 0x{0:x8}", result));
                }
                throw new MultimediaException(stringBuilder.ToString());
            }
        }

        #endregion

        #region DLL Import

        [DllImport("winmm.dll", EntryPoint = "midiInGetErrorText", CharSet = CharSet.Ansi, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.U4)]
        static extern int midiInGetErrorText([MarshalAs(UnmanagedType.U4)] int result,
            StringBuilder text, [MarshalAs(UnmanagedType.U4)] int bufferSize);

        [DllImport("winmm.dll", EntryPoint = "midiInGetNumDevs", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.U4)]
        static extern int midiInGetNumDevs();

        [DllImport("winmm.dll", EntryPoint = "midiInGetDevCaps", CharSet = CharSet.Ansi, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.U4)]
        static extern int midiInGetDevCaps([MarshalAs(UnmanagedType.U4)] int deviceID,
            ref MidiInputCaps midiInputCaps, [MarshalAs(UnmanagedType.U4)] int capsSize);

        [DllImport("winmm.dll", EntryPoint = "midiInOpen", CharSet = CharSet.Ansi, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.U4)]
        static extern int midiInOpen([MarshalAs(UnmanagedType.SysUInt)] out IntPtr midiInputHandle,
            [MarshalAs(UnmanagedType.U4)] int deviceID, [MarshalAs(UnmanagedType.FunctionPtr)] MidiInProc callback,
            [MarshalAs(UnmanagedType.U4)] int callbackInstance, [MarshalAs(UnmanagedType.U4)] MidiCallbackType callbackType);

        [DllImport("winmm.dll", EntryPoint = "midiInClose", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.U4)]
        static extern int midiInClose([MarshalAs(UnmanagedType.SysUInt)] IntPtr midiInputHandle);

        [DllImport("winmm.dll", EntryPoint = "midiInStart", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.U4)]
        static extern int midiInStart([MarshalAs(UnmanagedType.SysUInt)] IntPtr midiInputHandle);

        [DllImport("winmm.dll", EntryPoint = "midiInStop", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.U4)]
        static extern int midiInStop([MarshalAs(UnmanagedType.SysUInt)] IntPtr midiInputHandle);

        [DllImport("winmm.dll", EntryPoint = "midiInReset", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.U4)]
        static extern int midiInReset([MarshalAs(UnmanagedType.SysUInt)] IntPtr midiInputHandle);

        #endregion
    }
}
