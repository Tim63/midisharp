﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace MidiSharp.Win32
{
    public static class MultimediaResult
    {
        public static class SystemError
        {
            internal const int Base = 0;
            public const int NoError = 0;
            public const int Error = Base + 1;
            public const int BadDeviceId = Base + 2;
            public const int NotEnabled = Base + 3;
            public const int Allocated = Base + 4;
            public const int InvalidHandle = Base + 5;
            public const int NoDriver = Base + 6;
            public const int NoMemory = Base + 7;
            public const int NotSupported = Base + 8;
            public const int BadErrorNumber = Base + 9;
            public const int InvalidFlag = Base + 10;
            public const int InvalidParameter = Base + 11;
            public const int HandleBusy = Base + 12;
            public const int InvalidAlias = Base + 13;
            public const int BadDatabase = Base + 14;
            public const int KeyNotFound = Base + 15;
            public const int ReadError = Base + 16;
            public const int WriteError = Base + 17;
            public const int DeleteError = Base + 18;
            public const int ValueNotFound = Base + 19;
            public const int NoDriverCallback = Base + 20;
            public const int MoreData = Base + 21;
            public const int LastError = Base + 21;
        }

        public static class MidiError
        {
            internal const int Base = 64;
            public const int UnPrepared = Base + 0;
            public const int StillPlaying = Base + 1;
            public const int NoMap = Base + 2;
            public const int NotReady = Base + 3;
            public const int NoDevice = Base + 4;
            public const int InvalidSetup = Base + 5;
            public const int BadOpenMode = Base + 6;
            public const int DontContinue = Base + 7;
            public const int LastError = Base + 7;
        }
    }

    public class MultimediaException : Exception
    {
        public MultimediaException() : base() { }
        public MultimediaException(string message) : base(message) { }
        public MultimediaException(string message, Exception innerException) : base(message, innerException) { }
        protected MultimediaException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
