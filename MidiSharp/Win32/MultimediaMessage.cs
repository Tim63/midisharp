﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MidiSharp.Win32
{
    public static class MultimediaMessage
    {
        public static class MidiInputMessage
        {
            public const int Open = 0x3C1;
            public const int Close = 0x3C2;
            public const int Data = 0x3C3;
            public const int LongData = 0x3C4;
            public const int Error = 0x3C5;
            public const int LongError = 0x3C6;
            public const int MoreData = 0x3CC;
        }

        public static class MidiOutputMessage
        {
            public const int Open = 0x3C7;
            public const int Close = 0x3C8;
            public const int Done = 0x3C9;
            public const int PositionCallback = 0x3cA;
        }
    }
}
