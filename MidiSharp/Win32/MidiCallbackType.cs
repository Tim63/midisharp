﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MidiSharp.Win32
{
    enum MidiCallbackType
    {
        Typemask = 0x00070000,
        Null = 0x00000000,
        Window = 0x00010000,
        Task = 0x00020000,
        Function = 0x00030000,
        Thread = Task,
        Event = 0x00050000,
        MidiIOStatus = 0x00000020
    }

    public delegate void MidiInProc(IntPtr midiInHandle, int message, int instance, int parameter1, int parameter2);
}
