﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace MidiSharp.Win32
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct MidiOutputCaps
    {
        [MarshalAs(UnmanagedType.U2)]
        public ushort ManufacturerID;

        [MarshalAs(UnmanagedType.U2)]
        public ushort ProductID;

        [MarshalAs(UnmanagedType.U4)]
        public uint DriverVersion;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = Constants.MaxProductNameLength)]
        public string ProductName;

        [MarshalAs(UnmanagedType.U2)]
        public MidiDeviceType Technology;

        [MarshalAs(UnmanagedType.U2)]
        public ushort Voices;

        [MarshalAs(UnmanagedType.U2)]
        public ushort Notes;

        [MarshalAs(UnmanagedType.U2)]
        public ushort ChannelMask;

        [MarshalAs(UnmanagedType.U4)]
        public MidiSupports Support;
    }

    public enum MidiDeviceType : ushort
    {
        Hardware = 1,
        Synthesizer = 2,
        SquareSynth = 3,
        FMSynth = 4,
        MidiMapper = 5,
        Wavetable = 6,
        SoftwareSynth = 7
    }

    [Flags]
    public enum MidiSupports : uint
    {
        Volume = 0x0001,
        LRVolume = 0x0002,
        Cache = 0x0004,
        Stream = 0x0008
    }
}
