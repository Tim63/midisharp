﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace MidiSharp.Win32
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct MidiInputCaps
    {
        [MarshalAs(UnmanagedType.U2)]
        public ushort ManufacturerID;

        [MarshalAs(UnmanagedType.U2)]
        public ushort ProductID;

        [MarshalAs(UnmanagedType.U4)]
        public uint DriverVersion;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = Constants.MaxProductNameLength)]
        public string ProductName;
    }
}
