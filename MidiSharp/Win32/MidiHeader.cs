﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace MidiSharp.Win32
{
    [StructLayout(LayoutKind.Sequential)]
    struct MidiHeader
    {
        [MarshalAs(UnmanagedType.SysUInt)]
        public IntPtr Data;

        [MarshalAs(UnmanagedType.U4)]
        public uint BufferLength;

        [MarshalAs(UnmanagedType.U4)]
        public uint BytesRecorded;

        [MarshalAs(UnmanagedType.U4)]
        public uint User;

        [MarshalAs(UnmanagedType.U4)]
        public MidiHeaderFlags Flags;

        [MarshalAs(UnmanagedType.SysUInt)]
        public IntPtr Next;

        [MarshalAs(UnmanagedType.SysUInt)]
        public IntPtr ReservedForDriver;

        [MarshalAs(UnmanagedType.U4)]
        public uint Offset;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public IntPtr[] ReservedForMultimediaResult;
    }

    [Flags]
    public enum MidiHeaderFlags : uint
    {
        None = 0x00000000,
        Done = 0x00000001,
        Prepared = 0x00000002,
        InQueue = 0x00000004,
        IsStream = 0x00000008,
    }
}
