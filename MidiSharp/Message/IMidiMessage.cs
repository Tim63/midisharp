﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MidiSharp.Message
{
    public interface IMidiMessage
    {
        float TimeStamp { get; }

        bool IsLongMessage { get; }

        byte[] Encode();
    }

    public static class StatusByte
    {
        public const int MaxValue = 0xff;
        public const int MinValue = 0x80;

        public static byte Validate(byte data)
        {
            if (data < MinValue || data > MaxValue)
            {
                throw new ArgumentOutOfRangeException();
            }
            return data;
        }
    }

    public static class DataByte
    {
        public const int MaxValue = 0x7f;
        public const int MinValue = 0x00;

        public static byte Validate(byte data)
        {
            if (data < MinValue || data > MaxValue)
            {
                throw new ArgumentOutOfRangeException();
            }
            return data;
        }
    }
}
