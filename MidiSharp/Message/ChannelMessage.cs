﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MidiSharp.Message
{
    public abstract class ChannelMessage : IMidiMessage
    {
        public Channel Channel { get; private set; }

        public ChannelMessage(Channel channel, float timeStamp)
        {
            this.Channel = channel;
            this.timeStamp = timeStamp;
        }

        #region IMidiMessage

        bool IMidiMessage.IsLongMessage { get { return false; } }

        float IMidiMessage.TimeStamp { get { return timeStamp; } }
        float timeStamp;

        public abstract byte[] Encode();

        #endregion
    }
}
