﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MidiSharp.Message
{
    public class NoteMessage : ChannelMessage
    {
        public Note Note { get; private set; }
        public bool IsNoteOnMessage { get; private set; }

        public NoteMessage(Note note, float timeStamp, bool isNoteOnMessage)
            : base(note.Channel, timeStamp)
        {
            this.Note = note;
            this.IsNoteOnMessage = isNoteOnMessage;
        }

        public NoteMessage(Note note, float timeStamp)
            : this(note, timeStamp, true)
        {
        }

        public NoteMessage(Channel channel, byte pitch, byte velocity, float timeStamp)
            : this (new Note(channel, pitch, velocity), timeStamp, true)
        {
        }

        public NoteMessage(Channel channel, byte pitch, byte velocity, float timeStamp, bool isNoteOnMessage)
            : this(new Note(channel, pitch, velocity), timeStamp, isNoteOnMessage)
        {
        }

        #region IMidiMessage

        public override byte[] Encode()
        {
            byte statusByte = StatusByte.Validate((byte)((IsNoteOnMessage ? 0x90 : 0x80) | (int)Channel));
            return new byte[] { statusByte, Note.Pitch, Note.Velocity };
        }

        #endregion
    }
}
