﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MidiSharp.Message
{
    public class MessageReceivedEventArgs : EventArgs
    {
        public byte[] Data { get; private set; }
        public int TimeStamp { get; private set; }

        public MessageReceivedEventArgs(byte[] data, int timeStamp)
        {
            this.Data = data;
            this.TimeStamp = timeStamp;
        }
    }
}
