﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MidiSharp.Message;

namespace MidiSharp
{
    public struct Note
    {
        public Channel Channel { get { return channel; } }
        Channel channel;

        public byte Pitch { get { return pitch; } }
        byte pitch;

        public byte Velocity { get { return velocity; } }
        byte velocity;

        public Note(Channel channel, byte pitch, byte velocity)
        {
            this.channel = channel.Validate();
            this.pitch = DataByte.Validate(pitch);
            this.velocity = DataByte.Validate(velocity);
        }
    }
}
