﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MidiSharp.Message;

namespace MidiSharp
{
    public static class Pitch
    {
        public const byte CMinus1 = 0;
        public const byte CMinus1Sharp = (byte)(CMinus1 + Accidental.Sharp);
        public const byte DMinus1Flat = (byte)(DMinus1 + Accidental.Flat);
        public const byte DMinus1 = 2;
        public const byte DMinus1Sharp = (byte)(DMinus1 + Accidental.Sharp);
        public const byte EMinus1Flat = (byte)(EMinus1 + Accidental.Flat);
        public const byte EMinus1 = 4;
        public const byte FMinus1 = 5;
        public const byte FMinus1Sharp = (byte)(FMinus1 + Accidental.Sharp);
        public const byte GMinus1Flat = (byte)(GMinus1 + Accidental.Flat);
        public const byte GMinus1 = 7;
        public const byte GMinus1Sharp = (byte)(GMinus1 + Accidental.Sharp);
        public const byte AMinus1Flat = (byte)(AMinus1 + Accidental.Flat);
        public const byte AMinus1 = 9;
        public const byte AMinus1Sharp = (byte)(AMinus1 + Accidental.Sharp);
        public const byte BMinus1Flat = (byte)(BMinus1 + Accidental.Flat);
        public const byte BMinus1 = 11;

        public const byte C0 = 12;
        public const byte C0Sharp = (byte)(C0 + Accidental.Sharp);
        public const byte D0Flat = (byte)(D0 + Accidental.Flat);
        public const byte D0 = 14;
        public const byte D0Sharp = (byte)(D0 + Accidental.Sharp);
        public const byte E0Flat = (byte)(E0 + Accidental.Flat);
        public const byte E0 = 16;
        public const byte F0 = 17;
        public const byte F0Sharp = (byte)(F0 + Accidental.Sharp);
        public const byte G0Flat = (byte)(G0 + Accidental.Flat);
        public const byte G0 = 19;
        public const byte G0Sharp = (byte)(G0 + Accidental.Sharp);
        public const byte A0Flat = (byte)(A0 + Accidental.Flat);
        public const byte A0 = 21;
        public const byte A0Sharp = (byte)(A0 + Accidental.Sharp);
        public const byte B0Flat = (byte)(B0 + Accidental.Flat);
        public const byte B0 = 23;

        public const byte C1 = 24;
        public const byte C1Sharp = (byte)(C1 + Accidental.Sharp);
        public const byte D1Flat = (byte)(D1 + Accidental.Flat);
        public const byte D1 = 26;
        public const byte D1Sharp = (byte)(D1 + Accidental.Sharp);
        public const byte E1Flat = (byte)(E1 + Accidental.Flat);
        public const byte E1 = 28;
        public const byte F1 = 29;
        public const byte F1Sharp = (byte)(F1 + Accidental.Sharp);
        public const byte G1Flat = (byte)(G1 + Accidental.Flat);
        public const byte G1 = 31;
        public const byte G1Sharp = (byte)(G1 + Accidental.Sharp);
        public const byte A1Flat = (byte)(A1 + Accidental.Flat);
        public const byte A1 = 33;
        public const byte A1Sharp = (byte)(A1 + Accidental.Sharp);
        public const byte B1Flat = (byte)(B1 + Accidental.Flat);
        public const byte B1 = 35;

        public const byte C2 = 36;
        public const byte C2Sharp = (byte)(C2 + Accidental.Sharp);
        public const byte D2Flat = (byte)(D2 + Accidental.Flat);
        public const byte D2 = 38;
        public const byte D2Sharp = (byte)(D2 + Accidental.Sharp);
        public const byte E2Flat = (byte)(E2 + Accidental.Flat);
        public const byte E2 = 40;
        public const byte F2 = 41;
        public const byte F2Sharp = (byte)(F2 + Accidental.Sharp);
        public const byte G2Flat = (byte)(G2 + Accidental.Flat);
        public const byte G2 = 43;
        public const byte G2Sharp = (byte)(G2 + Accidental.Sharp);
        public const byte A2Flat = (byte)(A2 + Accidental.Flat);
        public const byte A2 = 45;
        public const byte A2Sharp = (byte)(A2 + Accidental.Sharp);
        public const byte B2Flat = (byte)(B2 + Accidental.Flat);
        public const byte B2 = 47;

        public const byte C3 = 48;
        public const byte C3Sharp = (byte)(C3 + Accidental.Sharp);
        public const byte D3Flat = (byte)(D3 + Accidental.Flat);
        public const byte D3 = 50;
        public const byte D3Sharp = (byte)(D3 + Accidental.Sharp);
        public const byte E3Flat = (byte)(E3 + Accidental.Flat);
        public const byte E3 = 52;
        public const byte F3 = 53;
        public const byte F3Sharp = (byte)(F3 + Accidental.Sharp);
        public const byte G3Flat = (byte)(G3 + Accidental.Flat);
        public const byte G3 = 55;
        public const byte G3Sharp = (byte)(G3 + Accidental.Sharp);
        public const byte A3Flat = (byte)(A3 + Accidental.Flat);
        public const byte A3 = 57;
        public const byte A3Sharp = (byte)(A3 + Accidental.Sharp);
        public const byte B3Flat = (byte)(B3 + Accidental.Flat);
        public const byte B3 = 59;
        public const byte C4 = 60; // Center
        public const byte C4Sharp = (byte)(C4 + Accidental.Sharp);
        public const byte D4Flat = (byte)(D4 + Accidental.Flat);
        public const byte D4 = 62;
        public const byte D4Sharp = (byte)(D4 + Accidental.Sharp);
        public const byte E4Flat = (byte)(E4 + Accidental.Flat);
        public const byte E4 = 64;
        public const byte F4 = 65;
        public const byte F4Sharp = (byte)(F4 + Accidental.Sharp);
        public const byte G4Flat = (byte)(G4 + Accidental.Flat);
        public const byte G4 = 67;
        public const byte G4Sharp = (byte)(G4 + Accidental.Sharp);
        public const byte A4Flat = (byte)(A4 + Accidental.Flat);
        public const byte A4 = 69;
        public const byte A4Sharp = (byte)(A4 + Accidental.Sharp);
        public const byte B4Flat = (byte)(B4 + Accidental.Flat);
        public const byte B4 = 71;

        public const byte C5 = 72;
        public const byte C5Sharp = (byte)(C5 + Accidental.Sharp);
        public const byte D5Flat = (byte)(D5 + Accidental.Flat);
        public const byte D5 = 74;
        public const byte D5Sharp = (byte)(D5 + Accidental.Sharp);
        public const byte E5Flat = (byte)(E5 + Accidental.Flat);
        public const byte E5 = 76;
        public const byte F5 = 77;
        public const byte F5Sharp = (byte)(F5 + Accidental.Sharp);
        public const byte G5Flat = (byte)(G5 + Accidental.Flat);
        public const byte G5 = 79;
        public const byte G5Sharp = (byte)(G5 + Accidental.Sharp);
        public const byte A5Flat = (byte)(A5 + Accidental.Flat);
        public const byte A5 = 81;
        public const byte B5Flat = (byte)(B5 + Accidental.Flat);
        public const byte B5 = 83;

        public const byte C6 = 84;
        public const byte C6Sharp = (byte)(C6 + Accidental.Sharp);
        public const byte D6Flat = (byte)(D6 + Accidental.Flat);
        public const byte D6 = 86;
        public const byte D6Sharp = (byte)(D6 + Accidental.Sharp);
        public const byte E6Flat = (byte)(E6 + Accidental.Flat);
        public const byte E6 = 88;
        public const byte F6 = 89;
        public const byte F6Sharp = (byte)(F6 + Accidental.Sharp);
        public const byte G6Flat = (byte)(G6 + Accidental.Flat);
        public const byte G6 = 91;
        public const byte G6Sharp = (byte)(G6 + Accidental.Sharp);
        public const byte A6Flat = (byte)(A6 + Accidental.Flat);
        public const byte A6 = 93;
        public const byte A6Sharp = (byte)(A6 + Accidental.Sharp);
        public const byte B6Flat = (byte)(B6 + Accidental.Flat);
        public const byte B6 = 95;

        public const byte C7 = 96;
        public const byte C7Sharp = (byte)(C7 + Accidental.Sharp);
        public const byte D7Flat = (byte)(D7 + Accidental.Flat);
        public const byte D7 = 98;
        public const byte D7Sharp = (byte)(D7 + Accidental.Sharp);
        public const byte E7Flat = (byte)(E7 + Accidental.Flat);
        public const byte E7 = 100;
        public const byte F7 = 101;
        public const byte F7Sharp = (byte)(F7 + Accidental.Sharp);
        public const byte G7Flat = (byte)(G7 + Accidental.Flat);
        public const byte G7 = 103;
        public const byte G7Sharp = (byte)(G7 + Accidental.Sharp);
        public const byte A7Flat = (byte)(A7 + Accidental.Flat);
        public const byte A7 = 105;
        public const byte A7Sharp = (byte)(A7 + Accidental.Sharp);
        public const byte B7Flat = (byte)(B7 + Accidental.Flat);
        public const byte B7 = 107;

        public const byte C8 = 108;
        public const byte C8Sharp = (byte)(C8 + Accidental.Sharp);
        public const byte D8Flat = (byte)(D8 + Accidental.Flat);
        public const byte D8 = 110;
        public const byte D8Sharp = (byte)(D8 + Accidental.Sharp);
        public const byte E8Flat = (byte)(E8 + Accidental.Flat);
        public const byte E8 = 112;
        public const byte F8 = 113;
        public const byte F8Sharp = (byte)(F8 + Accidental.Sharp);
        public const byte G8Flat = (byte)(G8 + Accidental.Flat);
        public const byte G8 = 115;
        public const byte G8Sharp = (byte)(G8 + Accidental.Sharp);
        public const byte A8Flat = (byte)(A8 + Accidental.Flat);
        public const byte A8 = 117;
        public const byte A8Sharp = (byte)(A8 + Accidental.Sharp);
        public const byte B8Flat = (byte)(B8 + Accidental.Flat);
        public const byte B8 = 119;

        public const byte C9 = 120;
        public const byte C9Sharp = (byte)(C9 + Accidental.Sharp);
        public const byte D9Flat = (byte)(D9 + Accidental.Flat);
        public const byte D9 = 122;
        public const byte D9Sharp = (byte)(D9 + Accidental.Sharp);
        public const byte E9Flat = (byte)(E9 + Accidental.Flat);
        public const byte E9 = 124;
        public const byte F9 = 125;
        public const byte F9Sharp = (byte)(F9 + Accidental.Sharp);
        public const byte G9 = 127;

        public static class DrumMap
        {
            public const byte BassDrum2 = 35;
            public const byte BassDrum1 = 36;
            public const byte SideStick = 37;
            public const byte SnareDrum1 = 38;
            public const byte HandClap = 39;
            public const byte SnareDrum2 = 40;
            public const byte LowTom2 = 41;
            public const byte ClosedHiHat = 42;
            public const byte LowTom1 = 43;
            public const byte PedalHiHat = 44;
            public const byte MidTom2 = 45;
            public const byte OpenHiHat = 46;
            public const byte MidTom1 = 47;
            public const byte HighTom2 = 48;
            public const byte CrashCymbal1 = 49;
            public const byte HighTom1 = 50;
            public const byte RideCymbal1 = 51;
            public const byte ChineseCymbal = 52;
            public const byte RideBell = 53;
            public const byte Tambourine = 54;
            public const byte SplashCymbal = 55;
            public const byte Cowbell = 56;
            public const byte CrashCymbal2 = 57;
            public const byte VibraSlap = 58;
            public const byte RideCymbal2 = 59;
            public const byte HighBongo = 60;
            public const byte LowBongo = 61;
            public const byte MuteHighConga = 62;
            public const byte OpenHighConga = 63;
            public const byte LowConga = 64;
            public const byte HighTimbale = 65;
            public const byte LowTimbale = 66;
            public const byte HighAgogo = 67;
            public const byte LowAgogo = 68;
            public const byte Cabasa = 69;
            public const byte Maracas = 70;
            public const byte ShortWhistle = 71;
            public const byte LongWhistle = 72;
            public const byte ShortGuiro = 74;
            public const byte LongGuiro = 74;
            public const byte Claves = 75;
            public const byte HighWoodBlock = 76;
            public const byte LowWoodBlock = 77;
            public const byte MuteCuica = 78;
            public const byte OpenCuica = 79;
            public const byte MuteTriangle = 80;
            public const byte OpenTriangle = 81;

            const byte MinValue = 35;
            const byte MaxValue = 81;
            public static byte Validate(byte purcussion)
            {
                if (purcussion < MinValue || purcussion > MaxValue)
                {
                    throw new ArgumentOutOfRangeException();
                }
                return purcussion;
            }
        }
    }

    public enum Accidental
    {
        DoubleFlat = -2,
        Flat = -1,
        Natural = 0,
        Sharp = 1,
        DoubleSgarp = 2,
    }
}
