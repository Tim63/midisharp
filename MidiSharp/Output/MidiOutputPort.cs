﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using MidiSharp.Win32;
using MidiSharp.Message;

namespace MidiSharp.Output
{
    public class MidiOutputPort : IDisposable
    {
        #region Fields

        const int MaxBufferSize = 64 * 1024;

        IntPtr handle = IntPtr.Zero;

        #endregion

        #region Properties

        public MidiOutputCaps Capabilities { get { return midiOutputCaps; } }
        MidiOutputCaps midiOutputCaps;

        public string ProductName { get { return midiOutputCaps.ProductName; } }

        public bool IsOpen { get { return isOpen; } }
        bool isOpen = false;

        public bool IsDisposed { get { return isDisposed; } }
        bool isDisposed = false;

        #endregion

        #region Constructor

        public MidiOutputPort(int portNumber)
        {
            midiOutputCaps = GetPortInfomation(portNumber);
            Open(portNumber);
        }

        private void Open(int portNumber)
        {
            if (!IsOpen)
            {
                CheckMultimediaResult(midiOutOpen(out handle, portNumber, null, 0, MidiCallbackType.Null));
                isOpen = true;
            }
        }

        ~MidiOutputPort()
        {
            Dispose(false);
        }

        #endregion

        #region IDisposable

        public void Close()
        {
            if (IsOpen)
            {
                CheckMultimediaResult(midiOutClose(handle));
                isOpen = false;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!IsDisposed)
            {
                Close();

                isDisposed = true;
            }
        }

        #endregion

        #region Send Message

        public void SendMessage(IMidiMessage message)
        {
            SendMessage(message.Encode());
        }

        public void SendMessage(byte[] data)
        {
            if (data.Length > 0)
            {
                if (data.Length <= 4)
                {
                    SendShortMessage(data);
                }
                else
                {
                    SendLongMessage(data);
                }
            }
        }

        private void SendShortMessage(byte[] data)
        {
            int message = 0;
            for (int i = 0; i < data.Length; i++)
            {
                message |= (data[i] << (8 * i));
            }
            CheckMultimediaResult(midiOutShortMsg(handle, message));
        }

        private void SendLongMessage(byte[] data)
        {
            if (data.Length > MaxBufferSize)
            {
                throw new ArgumentOutOfRangeException();
            }

            MidiHeader midiHeader = new MidiHeader();
            midiHeader.Flags = MidiHeaderFlags.None;
            midiHeader.BufferLength = (uint)data.Length;
            midiHeader.ReservedForMultimediaResult = new IntPtr[8];

            GCHandle dataHandle = GCHandle.Alloc(data, GCHandleType.Pinned);

            try
            {
                midiHeader.Data = dataHandle.AddrOfPinnedObject();

                SendMidiHeader(ref midiHeader);
            }
            finally
            {
                dataHandle.Free();
            }
        }

        private void SendMidiHeader(ref MidiHeader midiHeader)
        {
            int headerSize = Marshal.SizeOf(typeof(MidiHeader));

            CheckMultimediaResult(midiOutPrepareHeader(handle, ref midiHeader, headerSize));
            while ((midiHeader.Flags & MidiHeaderFlags.Prepared) == 0) { }

            CheckMultimediaResult(midiOutLongMsg(handle, ref midiHeader, headerSize));
            while ((midiHeader.Flags & MidiHeaderFlags.Done) == 0) { }

            CheckMultimediaResult(midiOutUnprepareHeader(handle, ref midiHeader, headerSize));
        }

        #endregion

        #region Static Methods

        public static int GetPortCount()
        {
            return midiOutGetNumDevs();
        }

        public static MidiOutputCaps GetPortInfomation(int portNumber)
        {
            var caps = new MidiOutputCaps();
            CheckMultimediaResult(midiOutGetDevCaps(portNumber, out caps, Marshal.SizeOf(typeof(MidiOutputCaps))));
            return caps;
        }

        private static void CheckMultimediaResult(int result)
        {
            if (result != MultimediaResult.SystemError.NoError)
            {
                var stringBuilder = new StringBuilder(Constants.MaxErrorStringLenght);
                if (midiOutGetErrorText(result, stringBuilder, stringBuilder.Capacity) != MultimediaResult.SystemError.NoError)
                {
                    throw new MultimediaException(string.Format("Failed to get error text. Code : 0x{0:x8}", result));
                }
                throw new MultimediaException(stringBuilder.ToString());
            }
        }

        #endregion

        #region DLL Import

        [DllImport("winmm.dll", EntryPoint = "midiOutGetErrorText", CharSet = CharSet.Ansi, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.U4)]
        static extern int midiOutGetErrorText([MarshalAs(UnmanagedType.U4)] int result,
            StringBuilder text, [MarshalAs(UnmanagedType.U4)] int bufferSize);

        [DllImport("winmm.dll", EntryPoint = "midiOutGetNumDevs", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.U4)]
        static extern int midiOutGetNumDevs();

        [DllImport("winmm.dll", EntryPoint = "midiOutGetDevCaps", CharSet = CharSet.Ansi, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.U4)]
        static extern int midiOutGetDevCaps([MarshalAs(UnmanagedType.U4)] int deviceID,
            out MidiOutputCaps midiOutputCaps, [MarshalAs(UnmanagedType.U4)] int capsSize);

        [DllImport("winmm.dll", EntryPoint = "midiOutOpen", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.U4)]
        static extern int midiOutOpen([MarshalAs(UnmanagedType.SysUInt)] out IntPtr midiOutputHandle,
            [MarshalAs(UnmanagedType.U4)] int deviceID, [MarshalAs(UnmanagedType.FunctionPtr)] Delegate callback,
            [MarshalAs(UnmanagedType.U4)] int callbackInstance, [MarshalAs(UnmanagedType.U4)] MidiCallbackType callbackType);

        [DllImport("winmm.dll", EntryPoint = "midiOutClose", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.U4)]
        static extern int midiOutClose([MarshalAs(UnmanagedType.SysUInt)] IntPtr midiOutputHandle);

        [DllImport("winmm.dll", EntryPoint = "midiOutShortMsg", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.U4)]
        static extern int midiOutShortMsg([MarshalAs(UnmanagedType.SysUInt)] IntPtr midiOutputHandle,
            [MarshalAs(UnmanagedType.U4)] int message);

        [DllImport("winmm.dll", EntryPoint = "midiOutLongMsg", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.U4)]
        static extern int midiOutLongMsg([MarshalAs(UnmanagedType.SysUInt)] IntPtr midiOutputHandle,
            ref MidiHeader midiHeader, [MarshalAs(UnmanagedType.U4)] int headerSize);

        [DllImport("winmm.dll", EntryPoint = "midiOutPrepareHeader", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.U4)]
        static extern int midiOutPrepareHeader([MarshalAs(UnmanagedType.SysUInt)] IntPtr midiOutputHandle,
            ref MidiHeader midiHeader, [MarshalAs(UnmanagedType.U4)] int headerSize);

        [DllImport("winmm.dll", EntryPoint = "midiOutUnprepareHeader", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.U4)]
        static extern int midiOutUnprepareHeader([MarshalAs(UnmanagedType.SysUInt)] IntPtr midiOutputHandle,
            ref MidiHeader midiHeader, [MarshalAs(UnmanagedType.U4)] int headerSize);

        #endregion
    }
}
